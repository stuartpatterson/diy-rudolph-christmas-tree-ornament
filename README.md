# Rudolph Christmas Tree Ornament

[YouTube Video](https://youtu.be/8iXEibNfG_s)
with additional content provided for the project.

This KiCAD schematic and PCB should be used in addition to the YouTube video.

This work is licensed under the Creative Commons 
Attribution-NonCommercial 4.0 International License. To view a copy
of this license, visit [http://creativecommons.org/licenses/by-nc/4.0/](http://creativecommons.org/licenses/by-nc/4.0/)

Any action you take upon the information in my YouTube videos or related schematics/stl/source code/additional content is strictly 
at your own risk and I will not be liable for losses, damages, or injuries in connection to the use of the videos or the recreation 
of the projects in the videos.  I am NOT a professional Electrical Engineer, nor am I licensed as an EE.